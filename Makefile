%.tex: %.json tex.tpl .venv
	sed 's/\([&#]\)/\\\\\1/g' $< > $<tmp \
	&& .venv/bin/python template.py --env 'variable_start_string=\VAR{' --env 'variable_end_string=}' --template tex.tpl $<tmp \
		| sed -e 's/<em>/\\textbf{/g' -e 's/<\/em>/}/g' > $@ \
	&& rm -f $<tmp

%.pdf: %.tex
	pdflatex $<

%.html: %.json html.tpl .venv
	.venv/bin/python template.py --template html.tpl --output $@ $<

.venv:
	python -m venv .venv && .venv/bin/pip install jinja2

.PHONY: clean mrproper

clean:
	rm -rf .venv *.log *.out *.aux *.synctex.gz

mrproper:
	rm -f *.html *.tex *.pdf
