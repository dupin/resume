# resume

_build resumes in various output formats._

Here are examples in [html](https://dupin.frama.io/resume/example.html) and [pdf](https://dupin.frama.io/resume/example.pdf) formats.

## Quick start

1. Create your json resume file like the example in this repository ;
2. Let the gitlab pipeline build your resume ;

## Please note

- You can do manual build with this command : `make example.html` (replace "example" with the name of your json file, and "html" with the extension you want) ;
- html, pdf and tex are the only extensions actually available ;
- All json files will be built to all extensions available by the gitlab pipeline ;
- The main branch will publish all html and pdf resume to gitlab pages (trick: checkout pages settings for pointing your domain name to your staically hosted pages on gitlab) ;