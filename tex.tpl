\documentclass[fontsize=10pt]{tccv}
\usepackage[utf8]{inputenc}
\usepackage[sfdefault]{FiraSans}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage{hyperref}

{% raw %}
\newcommand{\ville}[2]{#1\ifstrempty{#2}{}{(#2)}}
\newcommand{\lien}[2]{\ifstrempty{#2}{#1}{\href{#2}{#1}}}
{% endraw %}


\begin{document}

\part[\VAR{basics.name}]{\VAR{basics.label}}

{% if work %}
\section{\VAR{title.work}}
\begin{eventlist}
{% for w in work[:4] %}
\item{\VAR{w.startDate|datetime_format("%m/%Y")}}
     {\lien{\VAR{w.name}}{\VAR{w.url}}, \ville{\VAR{w.location.city}}{\VAR{w.location.postalCode}}}
     {\VAR{w.position}}

	\VAR{w.summary}
	
	{% for h in w.highlights %}
		- \VAR{h}
	{% endfor %}
{% endfor %}
\end{eventlist}
{% endif %}

\personal
      [\VAR{basics.url}\VAR{__name__}.html]
      {\VAR{basics.location.address} \newline \VAR{basics.location.postalCode} -- \VAR{basics.location.city}}
      {\href{tel:\VAR{basics.phone.replace(" ", "")}}{\VAR{basics.phone}}}
      {\VAR{basics.email}}

{% if education %}
\section{\VAR{title.education}}
\begin{yearlist}

{% for e in education[:2] %}
\item[\VAR{e.area}]{\VAR{e.endDate}}
     {\VAR{e.studyType}}
     {\lien{\VAR{e.institution}}{\VAR{e.url}}, \ville{\VAR{e.location.city}}{\VAR{e.location.postalCode}}}
{% endfor %}

\end{yearlist}
{% endif %}

{% if skills %}
\section{\VAR{title.skills}}
\begin{description}
{% for s in skills[:6] %}
\item[\VAR{s.name}]~\\
		\VAR{s.keywords|join(', ')}
{% endfor %}
\end{description}
{% endif %}

{% if languages %}
\section{\VAR{title.languages}}
\begin{factlist}
{% for l in languages[:1] %}
\item{\VAR{l.language}}{\VAR{l.fluency}}
{% endfor %}
\end{factlist}
{% endif %}

{% if bibliography %}
\section{\VAR{title.bibliography}}
\begin{factlist}
{% for b in bibliography[:5] %}
\item{\mbox{\lien{\VAR{b.title}}{\VAR{b.url}}}}{\VAR{b.author}}
{% endfor %}
\end{factlist}
{% endif %}

\end{document}
