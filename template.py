#!/usr/bin/python

import argparse
import datetime
import os
import sys
import json
import jinja2


def datetime_format(value, format="%d/%m/%Y", parse="%Y-%m-%d"):
    if isinstance(value, str):
        value = datetime.datetime.strptime(value, parse)
    return value.strftime(format)

jinja2.filters.FILTERS['datetime_format'] = datetime_format


parser = argparse.ArgumentParser()
parser.add_argument("--template", help="The template file, STDIN if omitted")
parser.add_argument("--output", help="The output file, STDOUT if omitted")
parser.add_argument("--env", help="Override jinja2 environment parameter", action='append')
parser.add_argument("data", help="The data file")
args = parser.parse_args()

template_filename = args.template
data_filename = args.data
output_filename = args.output

env = dict((e.split("=") for e in args.env)) if args.env else dict()


if template_filename:
    template_file = open(template_filename)
else:
    template_file = sys.stdin

template = template_file.read()

if template_file is not sys.stdin:
    template_file.close()

with open(data_filename) as data_file:
    data = json.load(data_file)

if output_filename:
    output_file = open(output_filename, 'w')
else:
    output_file = sys.stdout

data['__name__'] = os.path.splitext(data_filename)[0]
output_file.write(jinja2.Template(template, **env).render(data))

if output_file is not sys.stdout:
	output_file.close()
