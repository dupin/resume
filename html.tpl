<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>{{basics.name}}: {{basics.label}}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link href="default.css" rel="stylesheet" type="text/css" title="default">
	</head>
	<body>
		<header>
			<h1>{{basics.label}}</h1>
			<p id="name">{{basics.name}}</p>
		</header>
		<div id="content">
			<div id="main">
			  {% if work %}
				<section id="work">
					<h2>{{title.work}}</h2>
					<ul>
					{% for w in work %}
						<li>
								<h3>{{w.position}}</h3>
								<p>{{w.startDate|datetime_format("%m/%Y")}} | {% if w.url %}<a href="{{w.url}}">{{w.name}}</a>{% else %}{{w.name}}{% endif %}, {{w.location.city}}{% if w.location.postalCode %} ({{w.location.postalCode}}){% endif %}</p>
								<p>{{w.summary}}</p>	
								{% if w.highlights %}
								<ul class="highlights">
								{% for h in w.highlights %}
									<li>{{h}}</li>
								{% endfor %}
								</ul>
								{% endif %}
						</li>
					{% endfor %}			
					</ul>
				</section>
				{% endif %}
			</div>
			<div id="side">
				<address>
					<table>
						{% if basics.location.address ~ basics.location.postalCode ~ basics.location.city %}{% set line2 = [basics.location.postalCode, basics.location.city] | join(' - ') %}<tr><td class="vsep">🖂</td><td>{{ [basics.location.address, line2] | join('<br>') }}</td></tr>{% endif %}
						{% if basics.phone %}<tr><td class="vsep">📞</td><td><a href="tel:{{basics.phone}}">{{basics.phone}}</a></td></tr>{% endif %}
						{% if basics.email %}<tr><td class="vsep">@</td><td><a href="mailto:{{basics.email}}">{{basics.email}}</a></td></tr>{% endif %}
						<tr id="pdflink"><td class="vsep">☞</td><td><a href="{{__name__}}.pdf">PDF</a></td></tr>
					</table>
				</address>
				{% if skills %}
				<section id="skills">
					<h2>{{title.skills}}</h2>
					<ul>
					{% for s in skills %}
						<li>
							<h3>{{s.name}}</h3>
							<p>{{s.keywords|join(', ')}}</p>
						</li>	
					{% endfor %}
					</ul>
				</section>
				{% endif %}
				{% if education %}
				<section id="education">
					<h2>{{title.education}}</h2>
					<ul>
					{% for e in education %}
						<li>
							<h3>{{e.studyType}}</h3>
							<p>{{e.endDate}} | {% if e.url %}<a href="{{e.url}}">{{e.institution}}</a>{% else %}{{e.institution}}{% endif %}, {{e.location.city}}{% if e.location.postalCode %} ({{e.location.postalCode}}){% endif %}</p>
							{% if e.area %}<em>{{e.area}}</em>{% endif %}
						</li>
					{% endfor %}				
					</ul>
				</section>
				{% endif %}
				{% if languages %}
				<section id="languages">
				  <h2>{{title.languages}}</h2>
				  <ul>
				  {% for l in languages %}
				    <li><strong>{{l.language}}</strong> {{l.fluency}}</li>
				  {% endfor %}
				  </ul>
				</section>
				{% endif %}
				{% if bibliography %}
				<section id="bibliography">
					<h2>{{title.bibliography}}</h2>
					<ul>
					{% for b in bibliography %}
						<li><em><a href="{{b.url}}">{{b.title}}</a></em> {{b.author}}</li>
					{% endfor %}
					</ul>
				</section>
				{% endif %}
			</div>
		</div>
	</body>
</html>
